package com.itau.zipkin.cep.configuration;

import org.springframework.context.annotation.Bean;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;

public class RibbonConfiguration {

    @Bean
    public IRule iRule() {
        //aleatório
        return new RandomRule();

        //uma vez em cada máquina
        //return new RoundRobinRule();

        //por balanceamento de menor peso
        //return new WeightedResponseTimeRule();

    }

}

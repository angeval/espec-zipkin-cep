package com.itau.zipkin.cep.services;

import com.itau.zipkin.cep.clients.CepFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class CepService {
        @Autowired
        private CepFeign cepFeign;

        @NewSpan(name = "buscar-Cep-Service")
        public HashMap<String, Object> buscarCep(@SpanTag("cep") String cep) {
            return cepFeign.buscar(cep);
        }

    }
